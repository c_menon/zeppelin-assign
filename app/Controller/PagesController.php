<?php

App::uses('Controller', 'Controller');

/**
 * Pages Controller
 */
class PagesController extends AppController {

    public $components = array('DebugKit.Toolbar');

    public function display() {
        return $this->redirect(array('plugin' => 'website', 'controller' => 'DocumentUploadManagement', 'action' => 'uploadDocumentView'));
    }

}
