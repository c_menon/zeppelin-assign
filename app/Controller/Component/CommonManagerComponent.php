<?php

/**
 * CommonManager component
 * @author Manick
 * @property Component Auth
 */
class CommonManagerComponent extends Component {

    public $components = ['Auth', 'RequestHandler'];

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public function initialize(Controller $controller) {
        $this->User = ClassRegistry::init('User');
        $this->WebSession = ClassRegistry::init('WebSession');
        $this->request = Router::getRequest();
    }

}
?>