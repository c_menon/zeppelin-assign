<?php

App::uses('AppModel', 'Model');

class Temp extends AppModel {

    /**
     * Use table
     * @var mixed False or table name
     */
        public $useTable = 'temp';
     /*
     * Function to get temp List
     */
        public function getDetails($conditions = array()) {
        $tempDetails = $this->find('all', array('conditions' => $conditions));
        return $tempDetails;
    }
        

}
