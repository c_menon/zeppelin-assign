<?php
echo $this->Html->script('image-view');
echo $this->Html->css('image-view');
?>

<section class="content">
    <div class="col-lg-12 col-md-12 col-sm-12">
        <?php foreach($imageData as $imageDataInst){ ?>
        <div class="image__grid">
            <picture style="padding-left: 20px">
                <source media="(min-width: 760px)" srcset="<?php echo $imageDataInst['url'] ?>">
                <img class="single-image" src="<?php echo $imageDataInst['url'] ?>" alt="Upload">
            </picture>
        </div>
        <?php } ?>
    </div>
</section>
