<?php

//sudo chmod -R 777 /Applications/XAMPP/
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 */
App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
    /*
     * Function to Add if the Conditions are not met with existing row
     * Similar to Insert Ignore, with ignore based on customizable conditions
     */
    
    /*
     * Function to Add
     */

    public function add($conditions, $data) {
        if ($this->hasAny($conditions)) {
            return $this->find('first', array('conditions' => $conditions));
        } else {
            $this->create();
            return $this->save($data);
        }
    }

    /*
     * Function to Validate
     */

    public function validate($conditions) {
        if ($this->hasAny($conditions)) {
            return $this->find('first', array('conditions' => $conditions));
        } else {
            return false;
        }
    }
    
    public function getDetailsForId($id){
        return $this->validate(array('id'=>$id));
    }

    /*
     * Function to Update
     */

    public function update($conditions, $updateData, $addData = null) {
        if ($this->hasAny($conditions)) {
            $this->updateAll($updateData, $conditions);
            return $this->validate($conditions);
        } else if (!empty($addData)) {
            $this->create();
            return $this->save($addData);
        } else {
            return false;
        }
    }
    
    // Function to convert object to array recursively
    public function objectToArray($obj) {
        if (is_object($obj))
            $obj = (array) $obj;
        if (is_array($obj)) {
            $new = array();
            foreach ($obj as $key => $val) {
                $new[$key] = $this->objectToArray($val);
            }
        } else
            $new = $obj;
        return $new;
    }
    
    //
    public function makeDataForUpdate($data) {
        $return = array();
        foreach ( $data as $key => $value ) {
            $return[$key] = "'" . mysql_real_escape_string ( $value ) . "'";
        }
        return $return;
    }
}
