<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppModel', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class DocumentType extends AppModel {
    
    public $useTable = 'document_type';

    //get the supported document types
    public function getTypeId($uploadType) {
        $typeData = $this->find('first', array("conditions" => array("type" => $uploadType)));
        if ($typeData) {
            return $typeData['DocumentType']['id'];
        } else {
            return false;
        }
    }

}
