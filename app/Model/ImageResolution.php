<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppModel', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class ImageResolution extends AppModel {

    public $useTable = 'image_resolution';

    //get the supported image resolutions
    public function getImageResolutions() {
        $resolutionArray = array();
        $resolutionsArray = array();
        
        $resolutions = $this->find('all');
        if ($resolutions) {
            foreach ($resolutions as $resolutionsInst) {
                $resolutionArray['id'] = $resolutionsInst['ImageResolution']['id'];
                $resolutionArray['width'] = $resolutionsInst['ImageResolution']['width'];
                $resolutionArray['height'] = $resolutionsInst['ImageResolution']['height'];
                array_push($resolutionsArray, $resolutionArray);
            }
            return $resolutionsArray;
        }
        return false;
    }

}
