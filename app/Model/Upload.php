<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppModel', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class Upload extends AppModel {
    
    public $useTable = 'upload';
    
    //save the uploaded data
    public function saveData($uploadArray){
        $saveResult = $this->add(false, $uploadArray);
        if($saveResult){
            $addedData = $this->find('first', array('conditions' => array('name' => $uploadArray['name']), 'fields' => array("id")));
            return $addedData['Upload']['id'];
        } else {
            return false;
        }
    }

}

