<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppModel', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class CompressedImageUpload extends AppModel {

    public $useTable = 'compressed_image_upload';

    //save data to the table
    public function saveData($uploadArray) {
        $saveResult = $this->add(false, $uploadArray);
        if ($saveResult) {
            return true;
        } else {
            return false;
        }
    }

    //get images from the table for render
    public function getImages() {
        $imageArray = array();
        $finalImageArray = array();
        $fields = array('CompressedImageUpload.aws_url', 'ImageResolution.width');
        $params = array('fields' => $fields, 'joins' => array(
                array(
                    'table' => 'image_resolution', 'alias' => 'ImageResolution', 'type' => 'INNER', 'conditions' => array('ImageResolution.id = CompressedImageUpload.resolution_id')
                )
        ));
        $imageDetails = $this->find('all', $params);
        foreach($imageDetails as $imageDetailsInst){
            $imageArray['url'] = $imageDetailsInst['CompressedImageUpload']['aws_url'];
            $imageArray['width'] = $imageDetailsInst['ImageResolution']['width'];
            array_push($finalImageArray, $imageArray);
        }
        return($finalImageArray);
    }

}
