
<?php
/**
 *
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 */
$cakeDescription = __d('cake_dev', 'Dashboard');
?>
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="<?php echo APP.'webroot'.DS.'css'.DS.'bootstrap.min.css'; ?>" media="all" />
        <link rel="stylesheet" type="text/css" href="<?php echo APP.'webroot'.DS.'css'.DS.'bootstrap-theme.min.css'; ?>" media="all" />
        <link rel="stylesheet" type="text/css" href="<?php echo APP.'webroot'.DS.'css'.DS.'bootstrap-responsive.min.css'; ?>" media="all" />
        <link rel="stylesheet" type="text/css" href="<?php echo APP.'webroot'.DS.'css'.DS.'header.css'; ?>" media="all" />
        <link rel="stylesheet" type="text/css" href="<?php echo APP.'webroot'.DS.'css'.DS.'common.css'; ?>" media="all" />
        <link rel="stylesheet" type="text/css" href="<?php echo APP.'webroot'.DS.'css'.DS.'util.css'; ?>" media="all" />
        <link rel="stylesheet" type="text/css" href="<?php echo APP.'webroot'.DS.'css'.DS.'common_functions.css'; ?>" media="all" />
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php echo Configure::read('website.title'); ?>
        </title>
        <?php
        echo $this->Html->meta('icon');

        //echo $this->Html->css('cake.generic');
        echo $this->Html->css('bootstrap.min');
        echo $this->Html->css('bootstrap-theme.min');
        echo $this->Html->css('bootstrap-responsive.min');
        //echo $this->Html->css('template.min');
        echo $this->Html->css('header');
        echo $this->Html->css('common');
        echo $this->Html->css('footer');
        echo $this->Html->css('util');

        // Related to Table Sorting, Filtering & Pagination
        echo $this->Html->css('interactive-table/theme.blue');

        echo $this->Html->script('jquery.min');
        echo $this->Html->script('bootstrap.min');
        echo $this->Html->script('utils');
        echo $this->Html->script('common_functions');

        // Related to Table Sorting, Filtering & Pagination
        echo $this->Html->script('interactive-table/prettify');
        echo $this->Html->script('interactive-table/docs');
        echo $this->Html->script('interactive-table/jquery.tablesorter');
        echo $this->Html->script('interactive-table/jquery.tablesorter.pager');
        echo $this->Html->script('interactive-table/jquery.tablesorter.widgets');

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>  
    <body>
        <?php echo $this->fetch('content'); ?>
    </body>
</html>
