var addType = "";
var beacons = [];
var pathNodes = [];
var transitNodes = [];
var products = [];
var links = [];
var pathClickNumber = 0;
var newLink = {};
var recordVector = [];
var recordPointer = -1;
var maxRecordPointer;
var beaconIdNumber = 0;
var pathIdNumber = 0;
var productIdNumber = 0;
var imageSizeX,imageSizeY;
var highLightType,highLightIdx;
var floorList;
var transitId=-1;
var transitTag="";
var destNodes=[];
var transitLinks=[];

$(document).ready(function() {
    $("#imageMap").click(function(e){

        if(addType=="addLink"){
            $("#infoBox").html("Please click on some already made point.");
            return;
        }

        var image_left = $(this).offset().left;
        var click_left = e.pageX;
        var left_distance = click_left - image_left;

        var image_top = $(this).offset().top;
        var click_top = e.pageY;
        var top_distance = click_top - image_top;

        var mapper_width = $('#mapper').width();
        var imagemap_width = $('#imageMap').width();

        var mapper_height = $('#mapper').height();
        var imagemap_height = $('#imageMap').height();


        if((top_distance + mapper_height > imagemap_height) && (left_distance + mapper_width > imagemap_width)){
            $('#mapper').css("left", (click_left - mapper_width - image_left  ))
            .css("top",(click_top - mapper_height - image_top  ))
            .css("background-color","#000")
            .show();
        }

        else if(left_distance + mapper_width > imagemap_width){
            $('#mapper').css("left", (click_left - mapper_width - image_left  ))
            .css("top",top_distance)
            .css("background-color","#000")
            .show();
        }

        else if(top_distance + mapper_height > imagemap_height){
            $('#mapper').css("left", left_distance)
            .css("top",(click_top - mapper_height - image_top  ))
            .css("background-color","#000")
            .show();
        }

        else{
            $('#mapper').css("left",left_distance)
            .css("top",top_distance)
            .css("background-color","#000")
            .show();
        }

        $("#mapper").draggable({ containment: "parent" });
        $("#form_panel").hide();

    });
    
    //$("#imageMap").error().attr("src","/app/webroot/img/uploadimage.jpg");

});

var validateBeacon = function(){
    if(Number($("#beaconID").val()).toString()=="NaN"){
        $("#form_panel").html("beacon ID should be a number");
        $('#form_panel').show(0).delay(2000).hide(0);
        return false;
    }
    if(Number($("#major").val()).toString()=="NaN"){
        $("#form_panel").html("Major should be a number");
        $('#form_panel').show(0).delay(2000).hide(0);
        return false;
    }
    if(Number($("#minor").val()).toString()=="NaN"){
        $("#form_panel").html("Minor should be a number");
        $('#form_panel').show(0).delay(2000).hide(0);
        return false;
    }
    if($("#beaconID").val()==""){
        $("#form_panel").html("beacon ID should not be left blank");
        $('#form_panel').show(0).delay(2000).hide(0);
        return false;
    }
    if($("#major").val()==""){
        $("#form_panel").html("Major should not be left blank");
        $('#form_panel').show(0).delay(2000).hide(0);
        return false;
    }
    if($("#minor").val()==""){
        $("#form_panel").html("Minor should not be left blank");
        $('#form_panel').show(0).delay(2000).hide(0);
        return false;
    }
    for(var i=0;i<beacons.length;i++){
        if($("#beaconID").val()==beacons[i].ID){
            $("#form_panel").html("Beacon ID must be unique for a floor");
            $('#form_panel').show(0).delay(2000).hide(0);
            return false;
        }
    }
    return true;
}

var validateFloorDimensions = function(){
    if( (Number($("#floorWidth").val()).toString()=="NaN") || 
        (Number($("#floorWidth").val()).toString()=="NaN") ||
        (Number($("#floorWidth").val())<=0) ){
        $("#form_panel").html("Floor width and height must be a positive integer");
        $('#form_panel').show(0).delay(2000).hide(0);
        return false;
    }
    return true;
};

var validatePathNode = function(){
    if(Number($("#pathID").val()).toString()=="NaN"){
        $("#form_panel").html("path ID should be a number");
        $('#form_panel').show(0).delay(2000).hide(0);
        return false;
    }
    for(var i=0;i<pathNodes.length;i++){
        if(pathNodes[i].ID==$("#pathID").val()){
            $("#form_panel").html("path ID/transit ID should be unique for a floor");
            $('#form_panel').show(0).delay(2000).hide(0);
            return false;
        }
    }
    for(var i=0;i<transitNodes.length;i++){
        if(transitNodes[i].ID==$("#pathID").val()){
            $("#form_panel").html("path ID/transit ID should be unique for a floor");
            $('#form_panel').show(0).delay(2000).hide(0);
            return false;
        }
    }
    return true;
};

var validateProduct = function(){
    if(Number($("#pID").val()).toString()=="NaN"){
        $("#form_panel").html("product ID should be a number");
        $('#form_panel').show(0).delay(2000).hide(0);
        return false;
    }
    for(var i=0;i<products.length;i++){
        if(products[i].ID == $("#pID").val()){
            $("#form_panel").html("product ID should be unique for a floor");
            $('#form_panel').show(0).delay(2000).hide(0);
            return false;
        }
    }
    return true;
}


var addTag = function(){
    var position = $('#mapper').position();

    var pos_x = position.left;
    var pos_y = position.top;
    var pos_width = $('#mapper').width();
    var pos_height = $('#mapper').height();
    if(addType=="beacon"){
        $("#beaconID").val(beaconIdNumber);
        beaconIdNumber++;
        if(validateBeacon()==false){
            return;
        }
        var newBeacon = {};
        newBeacon.x = pos_x;
        newBeacon.y = pos_y;
        newBeacon.ID = $("#beaconID").val();
        newBeacon.major = $("#major").val();
        newBeacon.minor = $("#minor").val();
        beacons[beacons.length] = newBeacon;
    }
    else if(addType=="pathNode"){
        $("#pathID").val(pathIdNumber);
        pathIdNumber++;
        if(validatePathNode()==false){
            return;
        }
        var newPathNode = {};
        newPathNode.x = pos_x;
        newPathNode.y = pos_y;
        newPathNode.trans = false;
        newPathNode.ID = $("#pathID").val();
        newPathNode.pathTag = $("#pathTag").val();
        pathNodes[pathNodes.length] = newPathNode;
    }
    else if(addType=="transitNode"){
        $("#pathID").val(pathIdNumber);
        pathIdNumber++;
        if(validatePathNode()==false){
            return;
        }
        var newTransitNode = {};
        newTransitNode.x = pos_x;
        newTransitNode.y = pos_y;
        newTransitNode.trans = true;
        newTransitNode.ID = $("#pathID").val();
        newTransitNode.pathTag = $("#pathTag").val();
        for(var i in floorList){
            if(floorList[i]["id"]==document.getElementById("floorList").value){
                if(transitId==-1){
                    xCoordinate=document.getElementById("transitCreate").style.left;
                    xCoordinate=xCoordinate.substring(0,xCoordinate.length-2);
                    yCoordinate=document.getElementById("transitCreate").style.top;
                    yCoordinate=yCoordinate.substring(0,yCoordinate.length-2);
                    destNodes[destNodes.length]={"floorId":floorList[i]["id"],"id":nextTransitId,"tag":document.getElementById("pathTag2").value,"xCoordinate":((xCoordinate-5)/newscale),"yCoordinate":((yCoordinate-95)/newscale),"trans":true};
                    transitLinks[transitLinks.length]={"sourceType":"pnode","destinationType":"pnode","sourceId":parseInt(newTransitNode.ID),"destId":nextTransitId,"sourceFloorId":floorId,"destFloorId":floorList[i]["id"],"linkType":"TransitLink","weight":1};
                }
                else
                    transitLinks[transitLinks.length]={"sourceType":"pnode","destinationType":"pnode","sourceId":parseInt(newTransitNode.ID),"destId":transitId,"sourceFloorId":floorId,"destFloorId":floorList[i]["id"],"linkType":"TransitLink","weight":1};
                break;
            }
        }
        transitId=-1;
        transitNodes[transitNodes.length] = newTransitNode;
    }
    else if(addType=="product"){
        var currentProduct = JSON.parse($("#form_panel #selectedProduct").val());
        console.log(currentProduct);
        $("#pID").val(productIdNumber);
        productIdNumber++;
        $("#pCategory").val(currentProduct['productcategoryid']);
        $("#pName").val(currentProduct['name']);
        if(validateProduct()==false){
            return;
        }
        var newProduct = {};
        newProduct.x = pos_x;
        newProduct.y = pos_y;
        newProduct.ID = $("#pID").val();
        newProduct.name = $("#pName").val();
        newProduct.category = $("#pCategory").val();
        console.log(currentProduct["id"]);
        newProduct.productSectionID = currentProduct["id"];
        products[products.length] = newProduct;
    }
    else {
        console.log("Something went wrong");
    }
    $("#mapper").hide();
    $("#form_panel").hide();
    $("#debugFooter").html("Beacons : " + JSON.stringify(beacons) + " <br/> pathNodes : "  + JSON.stringify(pathNodes) + " <br/> products : " + JSON.stringify(products) );
    saveState();
    showTags();
};

var openDialog = function(){
    var panel = $("#form_panel");
    if(addType=="beacon"){
        panel.empty();
        panel.html(" <div class='row'> <div class='field required'><input type='hidden' id='beaconID' /></div> <br/> <div class='label'>Major</div><div class='field'><input type='text' id='major' /></div><br><div class='label'>Minor</div><div class='field'><input type='text' id='minor' /></div></div><div class='row'><div class='label'></div><div class='field'><input type='button' value='Add Beacon' class='btn-xs' onclick='addTag()' /> <input type='button' value='Cancel ' class='btn-xs' onclick='cancelTag()' /></div></div> ");
    }
    else if(addType=="product"){
        panel.empty();
        panel.html($("#productDropDown").html());
        panel.append(" <div class='row'> <div class='field'><input type='hidden' id='pID' /></div><div class='field'><input type='hidden' id='pName' /></div><div class='field'><input type='hidden' id='pCategory' /></div></div><div class='row'><div class='field'><input type='button' class='btn-xs' value='Add Product' onclick='addTag()' /> <input type='button' value='Cancel ' class='btn-xs' onclick='cancelTag()' /> </div></div> ");
    }
    else if(addType=="pathNode"){
        panel.empty();
        panel.html(" <div class='row'> <div class='field'><input type='hidden' id='pathID' /></div><br/><div class='label'>Tag</div><div class='field'><input type='text' id='pathTag' /></div></div><div class='row'><div class='field'><input type='button' class='btn-xs' value='Add Path' onclick='addTag()' /> <input type='button' class='btn-xs' value='Cancel ' onclick='cancelTag()' /> </div></div> ");
    }
    else if(addType=="transitNode"){
        panel.empty();
        panel.width(420);
        panel.html(" <div class='row'> <div class='field'><input type='hidden' id='pathID' /></div><br/><div class='label'>Tag</div><div class='field'><input type='text' id='pathTag' /></div><br/><br/><div class='label'>Floor</div><div id='transitDiv'></div><script>setDropDown();</script></div><div class='row'><div class='field'><input type='button' id='transitButton' class='btn-xs' value='Add Transit' onclick='addTag()' style='visibility:hidden;'/> <input type='button' class='btn-xs' value='Cancel ' onclick='cancelTag()' /> </div></div> ");
    }
    else {
        panel.empty();
        panel.html("Please specify type of addition you want to make.");
    }
    panel.fadeIn("slow");
};

var cancelTag = function(){
    $("#mapper").hide();
    $("#form_panel").hide();
    showTags();
};

var showBeacon = function(x,y){
 //   alert(x.toString()+ " " + y.toString());
    $('#planetmap').append('<div class="tagged"  style="width:'+'12px'+';height:'+
        '12px'+';left:'+x+';top:'+y+';background-color:#F00; border-radius:50%; " ></div>');
    //console.log($('.tagged').position().left);
};

var showPathNode = function(x,y){
    //alert(x.toString()+ " " + y.toString());
    $('#planetmap').append('<div class="tagged"  style="width:'+'10px'+';height:'+
        '10px'+';left:'+x+';top:'+y+';background-color:#0F0" ></div>');
};
var showTransitNode = function(x,y){
    //alert(x.toString()+ " " + y.toString());
    $('#planetmap').append('<div class="tagged"  style="width:'+'10px'+';height:'+
        '10px'+';left:'+x+';top:'+y+';background-color:#4B0082" ></div>');
};

var showProduct = function(x,y){
    //alert(x.toString()+ " " + y.toString());
    $('#planetmap').append('<div class="tagged"  style="width:'+'12px'+';height:'+
        '12px'+';left:'+x+';top:'+y+';background-color:#00F" ><img src="/app/webroot/img/product-icon.png" height="12px" width="12px"></div>');
};

var GetCoordinates = function(type,ID){
    //console.log(type + " " + ID);
    var ans = {};
    if(type=="beacons"){
        for(var i=0;i<beacons.length;i++){
            if(beacons[i].ID==ID){
                ans.x =  beacons[i].x;
                ans.y =  beacons[i].y;
                return ans;
            }
        }
    } else if(type=="products"){
        for(var i=0;i<products.length;i++){
            if(products[i].ID==ID){
                ans.x =  products[i].x;
                ans.y =  products[i].y;
                return ans;
            }
        }
    } else if(type=="pathNodes"){
        for(var i=0;i<pathNodes.length;i++){
            if(pathNodes[i].ID==ID){
                ans.x =  pathNodes[i].x;
                ans.y =  pathNodes[i].y;
                return ans;
            }
        }
        for(var i=0;i<transitNodes.length;i++){
            if(transitNodes[i].ID==ID){
                ans.x =  transitNodes[i].x;
                ans.y =  transitNodes[i].y;
                return ans;
            }
        }
    } else if(type=="transitNodes"){
        for(var i=0;i<transitNodes.length;i++){
            if(transitNodes[i].ID==ID){
                ans.x =  transitNodes[i].x;
                ans.y =  transitNodes[i].y;
                return ans;
            }
        }
    } else {
        console.log("OH Snap!! Something went wrong!! :( ");
    }
};

var showLink = function(link){
    if(link.sourceType==null||link.sourceID==null||link.destType==null||link.destID==null)
        return;
    var sourceXY = GetCoordinates(link.sourceType,link.sourceID);
    var destXY = GetCoordinates(link.destType,link.destID);
//    console.log(sourceXY.x.toString() + " " + sourceXY.y.toString() + " " + destXY.x.toString() + " " + destXY.y.toString());
 //   console.log(link.sourceType + " " + link.destType);
    if((link.sourceType=="beacons" && link.destType=="pathNodes") || (link.sourceType=="pathNodes" && link.destType=="beacons") )
        $('#planetmap').line(sourceXY.x+5,sourceXY.y+5,destXY.x+5,destXY.y+5, {color:"red",zindex:"1000"});
    else if(link.sourceType=="pathNodes" && link.destType=='pathNodes')
        $('#planetmap').line(sourceXY.x+5,sourceXY.y+5,destXY.x+5,destXY.y+5, {color:"black",zindex:"1000"});
    else if( (link.sourceType=="pathNodes" && link.destType=="products") || (link.sourceType=="products" && link.destType=="pathNodes") )
        $('#planetmap').line(sourceXY.x+5,sourceXY.y+5,destXY.x+5,destXY.y+5, {color:"#00F",zindex:"1000"});
    else if( (link.sourceType=="pathNodes" && link.destType=="transitNodes") || (link.sourceType=="transitNodes" && link.destType=="pathNodes") )
        $('#planetmap').line(sourceXY.x+5,sourceXY.y+5,destXY.x+5,destXY.y+5, {color:"#black",zindex:"1000"});
    else
        console.log("OH Snap!! Something went wrong!! :( ");
};

var showHighLightedBeacon = function(x,y) {
    //console.log("YES");
    $('#planetmap').append('<div class="tagged" id="highLight" style="width:'+'14px'+';height:'+
        '12px'+';left:'+x+';top:'+y+';background-color:#000; border-radius:50%; " ></div>');
    $("#highLight").animate({'background-color': '#F00'}, 'slow');
}

var showHighLightedProduct = function(x,y) {
    $('#planetmap').append('<div class="tagged" id="highLight" style="width:'+'14px'+';height:'+
        '14px'+';left:'+x+';top:'+y+';background-color:#FF0;border-color:black;border-style:solid;border-width:1px" ><img src="/app/webroot/img/product-icon.png" height="12px" width="12px"></div>');
}

var showHighLightedPathNode = function(x,y) {
    $('#planetmap').append('<div class="tagged" id="highLight" style="width:'+'12px'+';height:'+
        '12px'+';left:'+x+';top:'+y+';background-color:#000" ></div>');
    $("#highLight").animate({'background-color': '#0F0'}, 'slow');
}

var showHighLightedTransitNode = function(x,y) {
    $('#planetmap').append('<div class="tagged" id="highLight" style="width:'+'12px'+';height:'+
        '12px'+';left:'+x+';top:'+y+';background-color:#FFF" ></div>');
    $("#highLight").animate({'background-color': '#4B0082'}, 'slow');
}


var showTags = function(){
    $("#planetmap").children("div").remove();
    for(var i=0;i<links.length;i++){
        showLink(links[i]);
    }
    for(var i=0;i<beacons.length;i++){
        if(highLightType=="beacons" && highLightIdx==i){
            showHighLightedBeacon(beacons[i].x,beacons[i].y);
        } else {
            showBeacon(beacons[i].x,beacons[i].y);
        }
    }
    for(var i=0;i<pathNodes.length;i++){
        if(highLightType=="pathNodes" && highLightIdx==i){
            showHighLightedPathNode(pathNodes[i].x,pathNodes[i].y);
        } else {
            showPathNode(pathNodes[i].x,pathNodes[i].y);
        }
    }
    for(var i=0;i<transitNodes.length;i++){
        if(highLightType=="transitNodes" && highLightIdx==i){
            showHighLightedTransitNode(transitNodes[i].x,transitNodes[i].y);
        } else {
            showTransitNode(transitNodes[i].x,transitNodes[i].y);
        }
    }
    for(var i=0;i<products.length;i++){
        if(highLightType=="products" && highLightIdx==i){
            showHighLightedProduct(products[i].x,products[i].y);
        } else {
            showProduct(products[i].x,products[i].y);
        }
    }
    //console.log("links length : " + links.length);
};

var loadImage = function (input) {
  if (input.files && input.files[0]) {
     var filerdr = new FileReader();
     filerdr.onload = function(e) {
         $('#imageMap').attr('src', e.target.result);
     }
     filerdr.readAsDataURL(input.files[0]);
 }
 imageSizeX = $('#imageMap').width();
 imageSizeY = $('#imageMap').height();
};

var setDropDown = function(){
    var div=document.getElementById("transitDiv");
    var str="<select id='floorList' onchange='changeFloor()'>";
    for(var floorcount=0;floorcount<floorList.length;floorcount++){
            str+="<option value='"+floorList[floorcount]["id"]+"'>"+floorList[floorcount]["name"]+"</option>";
    }
    str+="</select><br/><br/>";
    div.innerHTML=str;
    div.innerHTML+="<div id='transitImage'><div id='transitNodes'></div><img id='floorimg' onload='floorHandle()' width='400' src='"+floorList[0]["file"]+"'/></div><div class='label' stlye='width:200px;'>Tag Of Destination<br>Transit</div><div class='field' style='position:absolute;left:140px;'><input type='text' id='pathTag2' /><div id='pathLabel' style='display:none'></div></div>";
    changeFloor();
}
var floorHandle=function(){
    document.getElementById("floorimg").onclick=function(e){
        document.getElementById("transitButton").style.visibility="visible";
        var div=document.getElementById("transitCreate");
        document.getElementById("pathTag2").value=transitTag;
        document.getElementById("pathTag2").style.display="block";
        document.getElementById("pathLabel").style.display="none";
        transitId=-1;
        if(div){
            div.style.display="block";
            div.style.top=(e.offsetY+95)+"px";
            div.style.left=(e.offsetX+5)+"px";
        }
        else{
            document.getElementById("transitNodes").innerHTML+='<div id="transitCreate" onclick="clickTransit" style="position:absolute; width:'+'7px'+';height:'+'7px'+';left:'+(e.offsetX+5)+';top:'+(e.offsetY+95)+';background-color:#4B0082;z-index:3000; " ></div>';
        }
    }
}
var clickTransit=function(e){
    document.getElementById("transitButton").style.visibility="visible";
    div=document.getElementById("transitCreate");
    if(div){
        div.style.display="None";
    }
    var offsetX=e.pageX-$("#floorimg").offset().left;
    var offsetY=e.pageY-$("#floorimg").offset().top;
    jQuery(e.target).css("background-color","#FFF");
    jQuery(e.target).animate({'background-color': '#4B0082'}, 'slow');
    for(var i in data["pnodes"]){
        if(data["pnodes"][i]["trans"]==true){
            if((offsetX>=(data["pnodes"][i]["xCoordinate"]*newscale)-1) && (offsetX<=(data["pnodes"][i]["xCoordinate"]*newscale)+8) && (offsetY>=(data["pnodes"][i]["yCoordinate"]*newscale)-1) && (offsetY<=(data["pnodes"][i]["yCoordinate"]*newscale)+8)){
                transitTag=document.getElementById("pathTag2").value;
                document.getElementById("pathLabel").innerHTML=data["pnodes"][i]["tag"];
                document.getElementById("pathTag2").style.display="none";
                document.getElementById("pathLabel").style.display="block";
                transitId=data["pnodes"][i]["id"];
            }
        }
    }
    for(var i in destNodes){
        if(destNodes[i]["floorId"]==document.getElementById("floorList").value){
            if((offsetX>=(destNodes[i]["xCoordinate"]*newscale)-1) && (offsetX<=(destNodes[i]["xCoordinate"]*newscale)+8) && (offsetY>=(destNodes[i]["yCoordinate"]*newscale)-1) && (offsetY<=(destNodes[i]["yCoordinate"]*newscale)+8)){
                transitTag=document.getElementById("pathTag2").value;
                document.getElementById("pathLabel").innerHTML=data["pnodes"][i]["tag"];
                document.getElementById("pathTag2").style.display="none";
                document.getElementById("pathLabel").style.display="block";
                transitId=destNodes[i]["id"];
            }
        }
    }
    e.stopPropagation();
}

var changeFloor = function(){
    var val=document.getElementById("floorList").value;
    var src="";
    for(var floorcount=0;floorcount<floorList.length;floorcount++){
        if(floorList[floorcount]["id"]==val){
            src=floorList[floorcount]["file"];
            break;
        }
    }
    document.getElementById("transitImage").innerHTML="<div id='transitNodes'></div><img id='floorimg' onload='floorHandle()' width='400' src='"+src+"'/>";
    showNodesOnDialog(val);

}

var loadData = function(floorData,remainingFloorData,currentfloorid){
    console.log(floorData);
    floorList=remainingFloorData;
    floorId=currentfloorid;
    for(var i in floorList){
        floorList[i]["data"]=JSON.parse(floorList[i]["data"]);
    }
    console.log(floorList);
    imageSizeX = $('#imageMap').width();
    imageSizeY = $('#imageMap').height();
    if(floorData=="" || floorData==undefined){
        clearAll();
        return;
    }
    beacons = [];
    products = [];
    pathNodes = [];
    links = [];
    destNodes=[];
    transitNodes=[];
    transitLinks=[];
    transitId=-1;
    transitTag="";
    destNodes=[];
    transitLinks=[];
    for(var i=0;i<floorData["beacons"].length;i++){
        var newBeacon = {};
        newBeacon.x = floorData["beacons"][i].xCoordinate;
        newBeacon.y = floorData["beacons"][i].yCoordinate;
        newBeacon.major = floorData["beacons"][i].major;
        newBeacon.minor = floorData["beacons"][i].minor;
        newBeacon.ID = floorData["beacons"][i].id;
        beaconIdNumber = Math.max(beaconIdNumber,newBeacon.ID);
        beacons[beacons.length]= newBeacon;
    }
    for(var i=0;i<floorData["products"].length;i++){
        var newProduct = {};
        newProduct.x = floorData["products"][i].xCoordinate;
        newProduct.y = floorData["products"][i].yCoordinate;
        newProduct.ID = floorData["products"][i].id;
        productIdNumber = Math.max(productIdNumber,newProduct.ID);
        newProduct.name = floorData["products"][i].productName;
        newProduct.category = floorData["products"][i].category;
        newProduct.productSectionID = floorData["products"][i].productSectionId;
        products[products.length] = newProduct;
    }
    for(var i=0;i<floorData["pnodes"].length;i++){
        var newPathNode = {};
        newPathNode.ID = floorData["pnodes"][i].id;
        pathIdNumber = Math.max(pathIdNumber,newPathNode.ID);
        newPathNode.x = floorData["pnodes"][i].xCoordinate;
        newPathNode.y = floorData["pnodes"][i].yCoordinate;
        newPathNode.pathTag = floorData["pnodes"][i].tag;
        if(!('trans' in floorData["pnodes"][i]))
            newPathNode.trans==false;
        else
            newPathNode.trans=floorData["pnodes"][i].trans;
        if(newPathNode.trans)
            transitNodes[transitNodes.length]=newPathNode;
        else
            pathNodes[pathNodes.length] = newPathNode;
    }
    for(var i=0;i<floorData["relationships"].length;i++){
        var newLink = {};
        newLink.sourceType = changeBackType(floorData["relationships"][i].sourceType);
        newLink.sourceID = floorData["relationships"][i].sourceId;
        newLink.destType = changeBackType(floorData["relationships"][i].destinationType);
        newLink.destID = floorData["relationships"][i].destinationId;
        newLink.weight = floorData["relationships"][i].weight;
        links[links.length] = newLink;
    }
    for(var i in floorData["transientRelationship"]){
        var newLink={};
        newLink.sourceType=floorData["transientRelationship"][i].sourceType;
        newLink.destinationType=floorData["transientRelationship"][i].destinationType;
        newLink.sourceId=floorData["transientRelationship"][i].sourceId;
        newLink.destId=floorData["transientRelationship"][i].destId;
        newLink.sourceFloorId=floorData["transientRelationship"][i].sourceFloorId;
        newLink.destFloorId=floorData["transientRelationship"][i].destFloorId;
        newLink.linkType=floorData["transientRelationship"][i].linkType;
        newLink.weight=floorData["transientRelationship"][i].weight;
        transitLinks[transitLinks.length]=newLink;
    }
    var imagemap_width = $('#imageMap').width();
    var imagemap_height = $('#imageMap').height();
    $('#floorWidth').attr("value",imagemap_width*floorData["imageScaleX"]);
    $('#floorHeight').attr("value",imagemap_height*floorData["imageScaleY"]);
 //   console.log(beacons.length);
 //   console.log(products.length);
 //   console.log(links.length);
 //   console.log(pathNodes.length);
    //console.log(pathIdNumber);
    //console.log(beaconIdNumber);
    //console.log(productIdNumber);
    pathIdNumber++;
    beaconIdNumber++;
    productIdNumber++;
    recordVector = [];
    recordPointer = -1;
    saveState();
    showTags();
}

var removeActiveClass = function(){
    $("#addBeacon").removeClass("active");
    $("#addPathNode").removeClass("active");
    $("#addTransitNode").removeClass("active");
    $("#addProduct").removeClass("active");
    $("#addLink").removeClass("active");
    $("#addDelete").removeClass("active");
};

var changeAddTypeToBeacon = function(){
    cancelTag();
    addType = "beacon";
    removeActiveClass();
    $("#addBeacon").addClass("active");
};
var changeAddTypeToProduct = function(){
    cancelTag();
    addType = "product";
    removeActiveClass();
    $("#addProduct").addClass("active");
};
var changeAddTypeToPathNode = function(){
    cancelTag();
    addType = "pathNode";
    removeActiveClass();
    $("#addPathNode").addClass("active");
};
var changeAddTypeToTransitNode = function(){
    cancelTag();
    addType = "transitNode";
    removeActiveClass();
    $("#addTransitNode").addClass("active");
};
var changeAddTypeToLink = function() {
    cancelTag();
    addType = "addLink";
    removeActiveClass();
    $("#addLink").addClass("active");
};

var changeAddTypeToDelete = function() {
    cancelTag();
    addType = "addDelete";
    removeActiveClass();
    $("#addDelete").addClass("active");
}

var clearAll = function() {
    beacons = [];
    links = [];
    products = [];
    pathNodes = [];
    transitNodes = [];
    recordVector = [];
    recordPointer = -1;
    saveState();
    showTags();
};

var clearRoutes = function() {
    links = [];
    saveState();
    showTags();
};

var showInfo = function(type,idx) {
    if(type=="beacons"){
        $("#infoBox").html("ID : " + beacons[idx].ID + "<br/>" +  " Major : " + beacons[idx].major + " Minor : " + beacons[idx].minor);
    } else if(type=="products"){
        $("#infoBox").html("ID : " + products[idx].ID + "<br/>" +  " Name : " + products[idx].name + " Category : " + products[idx].category);
    } else if(type=="pathNodes"){
        $("#infoBox").html("ID : " + pathNodes[idx].ID + "<br/>" +  " Name : " + pathNodes[idx].pathType );
    } else if(type=="transitNodes"){
        $("#infoBox").html("ID : " + transitNodes[idx].ID + "<br/>" +  " Name : " + transitNodes[idx].pathType );
    } else {
        console.log("OH Snap!! Something went wrong!! :( ");
    }
};

var duplicateEdge = function(link){
    for(var i=0;i<links.length;i++){
        if(links[i]==link)
            return true;
    }
    return false;
};

var validLink = function(link){
    if(link.sourceType!="pathNodes" && link.destType!="pathNodes"){
        $("#form_panel").html("The destination must always be a Path Node");
        $('#form_panel').show(0).delay(2000).hide(0);
        return false;
    }
    if(link.sourceID==link.destID && link.sourceType==link.destType){
        $("#form_panel").html("Self loop edge");
        $('#form_panel').show(0).delay(2000).hide(0);
        return false;
    }
    if(duplicateEdge(link)==true){
        $("#form_panel").html("There is already an edge between these two.");
        $('#form_panel').show(0).delay(2000).hide(0);
        return false;
    }
    if(link.sourceType!="pathNodes" && link.sourceType!="transitNodes" && hasAnotherLink(link.sourceType,link.sourceID)){
        $("#form_panel").html("Only Path Nodes and Transit Nodes can have more than one link connecting them.");
        $('#form_panel').show(0).delay(2000).hide(0);
        return false;
    }
    return true;
};

var hasAnotherLink = function(type,id){
    for(var i in links){
        if(links[i].sourceType==type && links[i].sourceID==id){
            return true;
        }
    }
    return false;
}
var adjustLink = function(link){
    if(link.sourceType=="pathNodes" && link.destType=="beacons")
        reverseLink(link);
    if(link.sourceType=="pathNodes" && link.destType=="products")
        reverseLink(link);
    if(link.sourceType=="pathNodes" && link.destType=="transitNodes")
        reverseLink(link);
};

var calcWeight = function(link){
    var sourceXY = GetCoordinates(link.sourceType,link.sourceID);
    var destXY = GetCoordinates(link.destType,link.destID);
    var imagemap_width = $('#imageMap').width();
    var imagemap_height = $('#imageMap').height();
    imageScaleX = parseFloat($("#floorWidth").val()) / imagemap_width;
    imageScaleY = parseFloat($("#floorHeight").val()) / imagemap_height;
    console.log(( (sourceXY.x - destXY.x) * (sourceXY.x - destXY.x) * imageScaleX * imageScaleX + 
        (sourceXY.y - destXY.y) * (sourceXY.y - destXY.y) * imageScaleY * imageScaleY ));
    return Math.sqrt( (sourceXY.x - destXY.x) * (sourceXY.x - destXY.x) * imageScaleX * imageScaleX + 
        (sourceXY.y - destXY.y) * (sourceXY.y - destXY.y) * imageScaleY * imageScaleY );
};

var biDirectional = function(link){
    if ( (link.sourceType=="pathNodes") && (link.destType=="pathNodes") )
        return true;
    return false;
};

var reverseLink = function(link){
    var temp = link.sourceType;
    link.sourceType=link.destType;
    link.destType=temp;
    temp = link.sourceID;
    link.sourceID = link.destID;
    link.destID = temp;
};


var deleteNode = function(type,idx){
    var ID;
    var newTransitLinks = [];
    var newDestNodes = [];
    var remDestNodes = [];
    if(type=="beacons")
        ID = beacons[idx].ID;
    else if(type=="products")
        ID = products[idx].ID;
    else if(type=="pathNodes")
        ID = pathNodes[idx].ID;
    else if(type=="transitNodes")
        ID = transitNodes[idx].ID;
    else{
        console.log("OH Snap!!, Something went wrong ");
    }
    console.log(type + " " + idx + " " + ID);
    var remLinks = [];
    for(var i=0;i<links.length;i++){
        if( (links[i].sourceID==ID && links[i].sourceType==type) || 
            (links[i].destID==ID && links[i].destType==type) ){
        } else {
            remLinks[remLinks.length] = i;
        }
    }
    var newLinksArray = [];
    for(var i=0;i<remLinks.length;i++)
        newLinksArray[newLinksArray.length] = jQuery.extend(true, {}, links[remLinks[i]]);
    links = newLinksArray;
    if(type=="transitNodes"){
        var remLinks = [];
        for(var i=0;i<links.length;i++){
            if( (links[i].sourceID==ID && links[i].sourceType=="pathNodes") || 
                (links[i].destID==ID && links[i].destType=="pathNodes") ){
            } else {
                remLinks[remLinks.length] = i;
            }
        }
    var newLinksArray = [];
    for(var i=0;i<remLinks.length;i++)
        newLinksArray[newLinksArray.length] = jQuery.extend(true, {}, links[remLinks[i]]);
    links = newLinksArray;
        for(var i in destNodes){
            remDestNodes[i]=true;
        }
        for(var i in transitLinks){
            if(transitLinks[i].sourceFloorId==floorId){
                if((transitLinks[i].sourceId==ID && transitLinks[i].sourceType=="pnode")){
                    for(var j in destNodes){
                        if((transitLinks[i].destFloorId==destNodes[j].floorId && transitLinks[i].destId==destNodes[j].id)){
                            remDestNodes[j]=false;
                        }
                    }
                }
                else{
                    newTransitLinks[newTransitLinks.length]=jQuery.extend(true,{},transitLinks[i]);
                }
            }
            else if(transitLinks[i].destFloorId==floorId){
                if(!(transitLinks[i].sourceId==ID && transitLinks[i].sourceType=="pnode")){
                    newTransitLinks[newTransitLinks.length]=jQuery.extend(true,{},transitLinks[i]);
                }
            }
        }
        for(var i in destNodes){
            if(remDestNodes[i]){
                newDestNodes[newDestNodes.length]=jQuery.extend(true,{},destNodes[i])
            }
        }
        transitLinks=newTransitLinks;

    }
    destNodes=newDestNodes;
    if(type=="beacons")
        beacons.splice(idx,1);
    if(type=="products")
        products.splice(idx,1);
    if(type=="pathNodes")
        pathNodes.splice(idx,1);
    if(type=="transitNodes")
        transitNodes.splice(idx,1);

    showTags();
    saveState();
}

var highLight = function(type,idx) {
    highLightType = type;
    highLightIdx = idx;
};

$(".tagged").live("click",function(e){
        cancelTag();
        var img_scope_top = $("#imageMap").offset().top + $("#imageMap").height();
        var img_scope_left = $("#imageMap").offset().left + $("#imageMap").width();
        $(this).draggable({ containment:[$("#imageMap").offset().left,$("#imageMap").offset().top,img_scope_left,img_scope_top]  });

        var image_left = $("#imageMap").offset().left;
        var click_left = e.pageX;
        var x = click_left - image_left;

        var image_top = $("#imageMap").offset().top;
        var click_top = e.pageY;
        var y = click_top - image_top;

        console.log( "Coordinates : "  + x.toString() + " " + y.toString() );
        var type = "";
        var idx = -1;
        for(var i=0;i<beacons.length;i++){
            if(x<=beacons[i].x+12 && x>=beacons[i].x-1 && y<=beacons[i].y+12 && y>=beacons[i].y-1){
                type = "beacons";
                idx = i;
            }
        }
        if(idx==-1){        
            for(var i=0;i<products.length;i++){
                if(x<=products[i].x+12 && x>=products[i].x-1 && y<=products[i].y+12 && y>=products[i].y-1){
                    type = "products";
                    idx = i;
                }
            }

        }
        if(idx==-1){
            for(var i=0;i<pathNodes.length;i++){
                if(x<=pathNodes[i].x+10 && x>=pathNodes[i].x-1 && y<=pathNodes[i].y+10 && y>=pathNodes[i].y-1){
                    type = "pathNodes";
                    idx = i;
                }
            }
        }
        if(idx==-1){
            for(var i=0;i<transitNodes.length;i++){
                if(x<=transitNodes[i].x+10 && x>=transitNodes[i].x-1 && y<=transitNodes[i].y+10 && y>=transitNodes[i].y-1){
                    type = "transitNodes";
                    idx = i;
                }
            }
        }
        showInfo(type,idx);
        if(idx==-1){
            console.log("OH Snap!! Something went wrong! :( ");
            return;
        } else {
            console.log(type + " " + idx);
        }
        if(addType=="addDelete"){
            deleteNode(type,idx);
        }
        if(addType!="addDelete")
            highLight(type,idx);
        if(addType=="addLink"){
            if(pathClickNumber==0){
                newLink.sourceType=type;
                if(type=="beacons"){
                    console.log(beacons[idx].ID);
                    newLink.sourceID = beacons[idx].ID;
                }
                else if(type=="products"){
                    console.log(products[idx].ID);
                    newLink.sourceID = products[idx].ID;
                }
                else if(type=="pathNodes"){
                    console.log(pathNodes[idx].ID);
                    newLink.sourceID = pathNodes[idx].ID;
                }
                else if(type=="transitNodes"){
                    console.log(transitNodes[idx].ID);
                    newLink.sourceID = transitNodes[idx].ID;
                }
                pathClickNumber++;
            } else {
                newLink.destType=type;
                if(type=="beacons"){
                    console.log(beacons[idx].ID);
                    newLink.destID = beacons[idx].ID;
                }
                else if(type=="products"){
                    console.log(products[idx].ID);
                    newLink.destID = products[idx].ID;
                }
                else if(type=="pathNodes"){
                    console.log(pathNodes[idx].ID);
                    newLink.destID = pathNodes[idx].ID;
                }
                else if(type=="transitNodes"){
                    console.log(transitNodes[idx].ID);
                    newLink.destID = transitNodes[idx].ID;
                }
                pathClickNumber=0;
                adjustLink(newLink);
                if(validLink(newLink)==true){
                    links[links.length] = jQuery.extend(true, {}, newLink);
                    if(biDirectional(newLink)==true){
                        reverseLink(newLink);
                        links[links.length] = jQuery.extend(true, {}, newLink);
                    }
                    saveState();
                } else{
                    console.log("Please provide a valid path");
                }
            }
        }
        showTags();
});

var saveState = function(){
    console.log("saveState called");
    var newBeacons = [];
    var newLinks = [];
    var newProducts = [];
    var newPathNodes = [];
    var newTransitNodes = [];
    var newTransitLinks=[];
    var newDestNodes=[];
    for(var i=0;i<beacons.length;i++)
        newBeacons[newBeacons.length] = jQuery.extend(true, {}, beacons[i]);
    for(var i=0;i<links.length;i++)
        newLinks[newLinks.length] = jQuery.extend(true, {}, links[i]);
    for(var i=0;i<products.length;i++)
        newProducts[newProducts.length] = jQuery.extend(true, {}, products[i]);
    for(var i=0;i<pathNodes.length;i++)
        newPathNodes[newPathNodes.length] = jQuery.extend(true, {}, pathNodes[i]);
    for(var i=0;i<transitNodes.length;i++)
        newTransitNodes[newTransitNodes.length] = jQuery.extend(true, {}, transitNodes[i]);
    for(var i=0;i<transitLinks.length;i++)
        newTransitLinks[newTransitLinks.length] = jQuery.extend(true,{},transitLinks[i]);
    for(var i=0;i<destNodes.length;i++)
        newDestNodes[newDestNodes.length] = jQuery.extend(true,{},destNodes[i]);
    var newRecord = {};
    newRecord.beacons = newBeacons;
    newRecord.links = newLinks;
    newRecord.pathNodes = newPathNodes;
    newRecord.transitNodes = newTransitNodes;
    newRecord.products = newProducts;
    newRecord.transitLinks= newTransitLinks;
    newRecord.destNodes = newDestNodes;
    recordPointer++;
    maxRecordPointer = recordPointer;
    recordVector[recordPointer]=newRecord;
};

var loadState = function(){
    var currentRecord = jQuery.extend(true, {}, recordVector[recordPointer]);
    beacons = currentRecord.beacons;
    links = currentRecord.links;
    products = currentRecord.products;
    pathNodes = currentRecord.pathNodes;
    transitNodes = currentRecord.transitNodes;
    transitLinks = currentRecord.transitLinks;
    destNodes = currentRecord.transitLinks;
};

var undo =  function(){
    console.log(JSON.stringify(recordVector));
    console.log(recordPointer);
    if(recordPointer<=0) return;
    recordPointer--;
    loadState();
    showTags();
};

var redo = function(){
    console.log(JSON.stringify(recordVector));
    console.log(recordPointer);
    if(recordPointer>=maxRecordPointer)
        return;
    recordPointer++;
    loadState();
    showTags();
}

var findLinkType = function(link){
    if((link.sourceType=="pathNodes" || link.sourceType=="transitNodes") && (link.destType=="pathNodes" || link.destType=="transitNodes"))
        return "RouteLink";
    return "NearestPNode";
};

var changeType = function(type){
    if(type=="beacons") return "beacon";
    if(type=="pathNodes") return "pnode";
    if(type=="transitNodes") return "pnode";
    if(type=="products") return "product";
}

var changeBackType = function(type){
    if(type=="beacon") return "beacons";
    if(type=="pnode") return "pathNodes";
    if(type=="product") return "products";
}

var JStoAPI = function (){
    console.log("JStoAPI called");
    if(validateFloorDimensions()==false){
        return;
    }
    if(!validateItems()){
        $("#form_panel").html("Every node must have atleast one link connecting it.");
        $('#form_panel').show(0).delay(2000).hide(0);
        return;
    }
    //alert(JSON.stringify(products));
    var finalBeacons= [];
    var finalProducts = [];
    var finalLinks =[];
    var finalPathNodes =[];
    for(var i=0;i<beacons.length;i++){
        var newBeacon = {};
        newBeacon.id = beacons[i].ID;
        newBeacon.xCoordinate = beacons[i].x;
        newBeacon.yCoordinate = beacons[i].y;
        newBeacon.major = beacons[i].major;
        newBeacon.minor = beacons[i].minor;
        finalBeacons[finalBeacons.length] = newBeacon;
    }
    for(var i=0;i<products.length;i++){
        var newProduct = {};
        newProduct.id = products[i].ID;
        newProduct.xCoordinate = products[i].x;
        newProduct.yCoordinate = products[i].y;
        newProduct.category = products[i].category;
        newProduct.productName = products[i].name;
        newProduct.productSectionId = products[i].productSectionID;
        finalProducts[finalProducts.length] = newProduct;
    }
    for(var i=0;i<pathNodes.length;i++){
        var newPathNode = {};
        newPathNode.id = pathNodes[i].ID;
        newPathNode.xCoordinate = pathNodes[i].x;
        newPathNode.yCoordinate = pathNodes[i].y;
        newPathNode.tag = pathNodes[i].pathTag;
        newPathNode.trans=pathNodes[i].trans;
        finalPathNodes[finalPathNodes.length] = newPathNode;
    }
    for(var i=0;i<transitNodes.length;i++){
        var newPathNode = {};
        newPathNode.id = transitNodes[i].ID;
        newPathNode.xCoordinate = transitNodes[i].x;
        newPathNode.yCoordinate = transitNodes[i].y;
        newPathNode.tag = transitNodes[i].pathTag;
        newPathNode.trans=transitNodes[i].trans;
        finalPathNodes[finalPathNodes.length] = newPathNode;
    }
    for(var i=0;i<links.length;i++){
        var newLink1 = {};
        newLink1.sourceId = links[i].sourceID;
        newLink1.sourceType = changeType(links[i].sourceType);
        newLink1.destinationId = links[i].destID;
        newLink1.destinationType = changeType(links[i].destType);
        newLink1.linkType = findLinkType(links[i]);
        newLink1.weight = calcWeight(links[i]);
        finalLinks[finalLinks.length] = newLink1;
    }
    var imagemap_width = $('#imageMap').width();
    var imagemap_height = $('#imageMap').height();
    imageScaleX = parseFloat($("#floorWidth").val()) / imagemap_width;
    imageScaleY = parseFloat($("#floorHeight").val()) / imagemap_height;
    //alert(imageScaleX+"  " + imageScaleY);
    var returnVal = '{' + '"beacons":' + JSON.stringify(finalBeacons) + ',"pnodes":' + JSON.stringify(finalPathNodes) + 
        ',"products":' + JSON.stringify(finalProducts) + ',"relationships":'+ JSON.stringify(finalLinks) + ',"imageScaleX":' + 
        imageScaleX +  ',"imageScaleY":' + imageScaleY + ',"imageSizeX":' + imageSizeX + ',"imageSizeY":' + imageSizeY +
        ',"transientRelationship":'+JSON.stringify(transitLinks)+',"transientNodes":'+JSON.stringify(destNodes)+'}';
    //alert(returnVal);
    $("#floorDataFormVal").attr("value",returnVal);
    console.log(JSON.parse($("#floorDataFormVal").attr("value")));
    $("#floorDataForm").submit();

}
var validateItems=function(){
    for(var i in beacons){
        var hasLink=false;
        for(var j in links){
            if(links[j].sourceType=="beacons" && links[j].sourceID==beacons[i].ID)
                hasLink=true;
        }
        if(!hasLink)
            return false;
    }
    for(var i in products){
        var hasLink=false;
        for(var j in links){
            if(links[j].sourceType=="products" && links[j].sourceID==products[i].ID)
                hasLink=true;
        }
        if(!hasLink)
            return false;
    }
    for(var i in transitNodes){
        var hasLink=false;
        for(var j in links){
            if((links[j].sourceType=="transitNodes" || links[j].sourceType=="pathNodes") && links[j].sourceID==transitNodes[i].ID)
                hasLink=true;
            else if((links[j].destType=="transitNodes" || links[j].destType=="pathNodes") && links[j].destID==transitNodes[i].ID)
                hasLink=true;
        }
        if(!hasLink)
            return false;
    }
    for(var i in pathNodes){
        var hasLink=false;
        for(var j in links){
            if(links[j].sourceType=="pathNodes" && links[j].sourceID==pathNodes[i].ID)
                hasLink=true;
            else if(links[j].destType=="pathNodes" && links[j].destID==pathNodes[i].ID)
                hasLink=true;
        }
        if(!hasLink)
            return false;
    }
    return true;
}
var showBeaconInfo = function(idx){
    var x = beacons[idx].x;
    var y = beacons[idx].y;
    x = x+15;
    y = y+15;
    $('#planetmap').append('<div class="tagInfo"  style="z-index:2000;left:'+x+';top:'+y+';">' + '<b><u>Beacon</u></b><br/><b>Major</b>: '+beacons[idx].major+'<br/><b>Minor</b>: '+beacons[idx].minor+'</div>');
};

var showProductInfo = function(idx){
    var x = products[idx].x;
    var y = products[idx].y;
    x = x+15;
    y = y+15;
    $('#planetmap').append('<div class="tagInfo"  style="z-index:2000;left:'+x+';top:'+y+';">' + '<b><u>Product</u></b><br/><b>Name</b>: '+products[idx].name+'<!--<br/><b>Category</b>: '+products[idx].category+'--></div>');
};

var showPathNodeInfo = function(idx){
    var x = pathNodes[idx].x;
    var y = pathNodes[idx].y;
    x = x+15;
    y = y+15;
    $('#planetmap').append('<div class="tagInfo"  style="z-index:2000;left:'+x+';top:'+y+';">' + '<b><u>Path Node</u></b><br/><b>Tag</b>: '+pathNodes[idx].pathTag+'</div>');
};
var showTransitNodeInfo = function(idx){
    var x = transitNodes[idx].x;
    var y = transitNodes[idx].y;
    var destFloorId="";
    x = x+15;
    y = y+15;
    delimeterFlag=false;
    for(i in transitLinks){
        var name="";
        if(transitLinks[i]["sourceFloorId"]==floorId && transitLinks[i]["sourceId"]==transitNodes[idx].ID){
            for(var j in floorList){
                if(floorList[j]["id"]==transitLinks[i]["destFloorId"]){
                    name=floorList[j]["name"];
                    name=name.split("Name: ")[1];
                }
            }
            if(delimeterFlag){
                destFloorId+=",<br/>"+name;
            }
            else{
                destFloorId=name;
                delimeterFlag=true;
            }
        }
        else if(transitLinks[i]["destFloorId"]==floorId && transitLinks[i]["destId"]==transitNodes[idx].ID){
            for(var j in floorList){
                if(floorList[j]["id"]==transitLinks[i]["sourceFloorId"]){
                    name=floorList[j]["name"];
                    name=name.split("Name: ")[1];
                }
            }
            if(delimeterFlag){
                destFloorId+=",<br/>"+name;
            }
            else{
                destFloorId=name;
                delimeterFlag=true;
            }
        }
    }
    $('#planetmap').append('<div class="tagInfo"  style="z-index:2000;left:'+x+';top:'+y+';">' + '<b><u>Transit Node</u></b><br/><b>Tag</b>: '+transitNodes[idx].pathTag+'<br/><b>To</b>: '+destFloorId+'</div>');
    
};

$(".tagged").live("mouseover",function(e){
    var image_left = $("#imageMap").offset().left;
    var click_left = e.pageX;
    var x = click_left - image_left;

    var image_top = $("#imageMap").offset().top;
    var click_top = e.pageY;
    var y = click_top - image_top;

    console.log( "Coordinates : "  + x.toString() + " " + y.toString() );
    var type = "";
    var idx = -1;
    for(var i=0;i<beacons.length;i++){
        if(x<=beacons[i].x+12 && x>=beacons[i].x && y<=beacons[i].y+12 && y>=beacons[i].y){
            type = "beacons";
            idx = i;
        }
    }
    if(idx==-1){        
        for(var i=0;i<products.length;i++){
            if(x<=products[i].x+12 && x>=products[i].x && y<=products[i].y+12 && y>=products[i].y){
                type = "products";
                idx = i;
            }
        }
    }
    if(idx==-1){
        for(var i=0;i<pathNodes.length;i++){
            if(x<=pathNodes[i].x+10 && x>=pathNodes[i].x && y<=pathNodes[i].y+10 && y>=pathNodes[i].y){
                type = "pathNodes";
                idx = i;
            }
        }
    }
    if(idx==-1){
        for(var i=0;i<transitNodes.length;i++){
            if(x<=transitNodes[i].x+10 && x>=transitNodes[i].x && y<=transitNodes[i].y+10 && y>=transitNodes[i].y){
                type = "transitNodes";
                idx = i;
            }
        }
    }
    showInfo(type,idx);
    if(idx==-1){
        console.log("OH Snap!! Something went wrong!! :( ");
    } else {
        if(type=="beacons")
            showBeaconInfo(idx);
        else if(type=="products")
            showProductInfo(idx);
        else if(type=="pathNodes")
            showPathNodeInfo(idx);
        else if(type=="transitNodes")
            showTransitNodeInfo(idx);
        else
            console.log("OH Snap!! Something went wrong!! :( ");
    }
});

$(".tagged").live("mouseout",function(){
    console.log("out");
    $("#planetmap").find(".tagInfo").remove();
});

var showNodesOnDialog = function(idx){
    data="";
    transitImageDiv=document.getElementById("transitNodes");
    var imgPath="";
    for(var i in floorList){
        if(floorList[i]["id"]==idx){
            data=floorList[i]["data"];
            imgPath=floorList[i]["file"];
            break;
        }
    }
    var img=new Image();
    img.onload=function(){
        newscale=400/this.width;
        nextTransitId=0;
        for(var i in data["beacons"]){
            transitImageDiv.innerHTML+='<div style="position:absolute; width:'+'9px'+';height:'+'9px'+';left:'+(5+(data["beacons"][i]["xCoordinate"]*newscale))+';top:'+(95+(data["beacons"][i]["yCoordinate"]*newscale))+';background-color:#F00; border-radius:100%;z-index:3000; " ></div>';
        }
        for(var i in data["pnodes"]){
            if(data["pnodes"][i]["trans"]==true){
                transitImageDiv.innerHTML+='<div onclick="clickTransit(event)"style="position:absolute; width:'+'7px'+';height:'+'7px'+';left:'+(5+(data["pnodes"][i]["xCoordinate"]*newscale))+';top:'+(95+(data["pnodes"][i]["yCoordinate"]*newscale))+';background-color:#4B0082;z-index:3000; " ></div>';
                nextTransitId=Math.max(nextTransitId,parseInt(data["pnodes"][i]["id"]));
            }
            else{
                transitImageDiv.innerHTML+='<div style="position:absolute; width:'+'7px'+';height:'+'7px'+';left:'+(5+(data["pnodes"][i]["xCoordinate"]*newscale))+';top:'+(95+(data["pnodes"][i]["yCoordinate"]*newscale))+';background-color:#0F0;z-index:3000; " ></div>';
                nextTransitId=Math.max(nextTransitId,parseInt(data["pnodes"][i]["id"]));
            }
        }
        for(var i in destNodes){
            if(destNodes[i]["floorId"]==document.getElementById("floorList").value){
                transitImageDiv.innerHTML+='<div onclick="clickTransit(event)"style="position:absolute; width:'+'7px'+';height:'+'7px'+';left:'+(5+(destNodes[i]["xCoordinate"]*newscale))+';top:'+(95+(destNodes[i]["yCoordinate"]*newscale))+';background-color:#4B0082;z-index:3000; " ></div>';
                nextTransitId=Math.max(nextTransitId,parseInt(destNodes[i]["id"]));
            }
        }
        nextTransitId++;
        for(var i in data["products"]){
            transitImageDiv.innerHTML+='<div style="position:absolute; width:'+'9px'+';height:'+'9px'+';left:'+(5+(data["products"][i]["xCoordinate"]*newscale))+';top:'+(95+(data["products"][i]["yCoordinate"]*newscale))+';background-color:#00F; border-radius:50%;z-index:3000; " ><img src="/app/webroot/img/product-icon.png" height="7px" width="7px"></div>';
        }

    }
    img.src=imgPath;

}
