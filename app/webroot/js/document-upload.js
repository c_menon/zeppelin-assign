$(function () {
    var uploaded = '';
    var fileType = '';
    $('#uploaded').change(function () {
        uploaded = document.getElementById("uploaded").files[0].name;
        var nameSplit = uploaded.split(".");
        fileType = nameSplit[nameSplit.length - 1];

        var reader = new FileReader();
        reader.onload = function (event) {
            document.getElementById("uploaded").src = event.target.result;
        };
        reader.readAsDataURL(document.getElementById("uploaded").files[0]);
    });

    var uploadedData = {};
    $('#upload').click(function () {
        uploadedData['uploaded_file'] = 'upload_' + $.now() + '.' + fileType.toLowerCase();
        uploadedData['file_type'] = fileType.toLowerCase();
        uploadedData['uploaded_file_data'] = $('#uploaded').attr('src');
        var data = JSON.stringify(uploadedData);

        $.ajax({
            beforeSend: function () {
                $('#loadingmessage').show();
            },
            type: "POST",
            url: "/website/DocumentUploadManagement/receiveUploadData",
            dataType: "json",
            data: data,
            contentType: "application/json",
            success: function (response) {
                console.log(response);
                if (response.data == 'SUCCESS') {
                    alert('File Upload Successful');
                }
            },
            complete: function () {
                $('#loadingmessage').hide();
            },
            error: function () {
                alert('Some error occured! Please try later.');
            }
        });
    });
});
            