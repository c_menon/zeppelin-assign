$(document).ready(function() {
    $('#confirmLogout').click(function(event) {
        $.post("/website/logout", $('#signIn').serialize(), function(data, status) {
            if (data === '1') {
                var url = '/website/login';
                $(location).attr('href', url);
            }
            else {

            }
        });
    });
});

function removeAccountSection() {
    $('#accountSection').remove();
}