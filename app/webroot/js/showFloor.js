var showAllCustomersFlag=false;
var customerExists = false;
$(document).ready(function() {

});

var productX,productY,customerX,customerY,customerData,allCustomers,customerName,allProducts;

var init = function(productX1,productY1,customerX1,customerY1,customerData1,allCustomers1,floorData){
    productX = productX1;
    productY = productY1;
    customerX = customerX1;
    customerY = customerY1;
    customerData = customerData1;
    allCustomers = allCustomers1;
    allProducts = floorData['products'];
    //alert(JSON.stringify(products));
    showEverything();
}

var showEverything = function() {
    $("#planetmap").children("div").remove();
    customerName = customerData['firstname'] + " " + customerData["lastname"];
    for(var i=0;i<allCustomers.length;i++){
        if(allCustomers[i]['id']==customerData['id'])
            customerExists=true;
    }
    if(showAllCustomersFlag==false){
        showCustomer(customerX,customerY,customerName);
        if( (productX!=-1 && productY!=-1) && (productX!=undefined && productY!=undefined) )
            showAllProducts(productX,productY);
    }
    else{
        showAllCustomers();
        showAllProducts();
    }
};

var toggleView = function(){
    if(showAllCustomersFlag==false){
        $("#toggleButton").attr("value","Switch View to Single Customer");
        showAllCustomersFlag = true;
    }
    else{
        $("#toggleButton").attr("value","Switch View to All Customers");
        showAllCustomersFlag = false;
    }
    showEverything();
};

var showAllCustomers = function(){
    for(var i=0;i<allCustomers.length;i++)
        showCustomer(allCustomers[i]['cX'],allCustomers[i]['cY'],allCustomers[i]['customerFirstName']+" "+allCustomers[i]['customerLastName']);
    if(customerExists==false){
        showCustomer(customerX,customerY,customerName);
    }
};

var showAllProducts = function(){
    for(var i=0;i<allProducts.length;i++)
        showProduct(allProducts[i]['xCoordinate'],allProducts[i]['yCoordinate']);

};

var showCustomer = function(x,y){
    $('#planetmap').append('<div class="customer"  style="width:'+'15px'+';height:'+
        '15px'+';left:'+x+';top:'+y+';position:absolute;" ><img src="/app/webroot/img/man-icon.ico" height="15px" width="15px"></div>');
};

var showProduct = function(x,y){
    $('#planetmap').append('<div class="product"  style="width:'+'15px'+';height:'+
        '15px'+';left:'+x+';top:'+y+';background-color:#00F;position:absolute;" ><img src="/app/webroot/img/product-icon.png" height="15px" width="15px"></div>');
};


$(".customer").live("mouseover",function(e){
    var image_left = $("#imageMap").offset().left;
    var click_left = e.pageX;
    var x = click_left - image_left;

    var image_top = $("#imageMap").offset().top;
    var click_top = e.pageY;
    var y = click_top - image_top;

    console.log(x + " " + y);
    var idx=-1;
    for(var i=0;i<allCustomers.length;i++){
        if(x<=allCustomers[i]['cX']+15 && x>=allCustomers[i]['cX']-1 && y<=allCustomers[i]['cY']+15 && y>=allCustomers[i]['cY']-1){
            idx = i;
            break;
        }
    }
    if(idx==-1){
        console.log(customerX + " " + customerY);
        if(x<=customerX+15 && x>=customerX-1 && y<=customerY+15 && y>=customerY-1){        
            x = x+15;
            y = y+15;
            $('#planetmap').append('<div class="tagInfo"  style="z-index:2000;position:absolute;background-color:#FFF; padding:2px; border:1px solid black;left:'+x+';top:'+y+';">' 
            + '<b>Name</b>: '+ customerName + '</div>');
        }
        return;
    }

    x = x+15;
    y = y+15;
    $('#planetmap').append('<div class="tagInfo"  style="z-index:2000;position:absolute;background-color:#FFF; padding:2px; border:1px solid black;left:'+x+';top:'+y+';">' 
        + '<b>Name</b>: '+ allCustomers[idx]['customerFirstName']+' '+allCustomers[i]['customerLastName'] + '</div>');
});

$(".customer").live("mouseout",function(){
    console.log("out");
    $("#planetmap").find(".tagInfo").remove();
});


$(".product").live("mouseover",function(e){
    var image_left = $("#imageMap").offset().left;
    var click_left = e.pageX;
    var x = click_left - image_left;

    var image_top = $("#imageMap").offset().top;
    var click_top = e.pageY;
    var y = click_top - image_top;

    console.log(x + " " + y);
    var idx = -1;
    for(var i=0;i<allProducts.length;i++){
        //console.log(allProducts[i]['xCoordinate'] + " " + allProducts[i]['yCoordinate']);
        if(x<=allProducts[i]['xCoordinate']+15 && x>=allProducts[i]['xCoordinate']-1 && y<=allProducts[i]['yCoordinate']+15 && y>=allProducts[i]['yCoordinate']-1){
            idx = i;
            break;
        }
    }
    console.log(idx);
    if(idx==-1) return;

    x = x+15;
    y = y+15;

    $('#planetmap').append('<div class="tagInfo"  style="z-index:2000;position:absolute;background-color:#FFF; padding:2px; border:1px solid black;left:'+x+';top:'+y+';">' 
        + '<b>Name</b>: '+ allProducts[idx]['productName'] + '</div>');

});

$(".product").live("mouseout",function(){
    console.log("out");
    $("#planetmap").find(".tagInfo").remove();
});


