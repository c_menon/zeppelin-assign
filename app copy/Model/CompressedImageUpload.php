<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('AppModel', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class CompressedImageUpload extends AppModel {

    public $useTable = 'compressed_image_upload';

    public function saveData($uploadArray) {
        $saveResult = $this->add(false, $uploadArray);
        if ($saveResult) {
            return true;
        } else {
            return false;
        }
    }

}
