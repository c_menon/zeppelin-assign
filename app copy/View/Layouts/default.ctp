<?php

/**
 *
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 */
$cakeDescription = __d('cake_dev', 'Dashboard');

// user type menu
$userTypeMenu = isset($_COOKIE['userType']) ? $_COOKIE['userType'] : '';
?>
<html>
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
        </title>
        <?php
        echo $this->Html->meta('icon');    
        echo $this->Html->css('bootstrap.min');
        echo $this->Html->css('font-awesome.min');
        echo $this->Html->css('AdminLTE');
        echo $this->Html->css('common-theme');


        echo $this->Html->script('jquery');
        echo $this->Html->script('bootstrap.min');
        echo $this->Html->script('app');
        echo $this->Html->script('jquery.validate.min');
        echo $this->Html->script('utils');

        echo $this->fetch('meta');
        //echo $this->fetch('css');
        echo $this->fetch('script');
        ?>
        <meta name="viewport" content="width=device-width, initial-scale=1">
    </head>  
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <div class="content-wrapper">
                <div id="loadingmessage" style="display:none;">
                    <img src="/img/loading_1.gif" style="z-index: 2000;vertical-align: middle;position: absolute;top: 50%;left: 50%;"/>
                </div>
                <?php echo $this->fetch('content'); ?>
            </div>
        </div>
        <!-- ./wrapper -->
    </body>
</html>
