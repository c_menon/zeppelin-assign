<?php
echo $this->Html->script('document-upload');
echo $this->Html->css('jquery.fileuploader');
echo $this->Html->css('jquery.fileuploader-theme-thumbnails');
echo $this->Html->script('jquery.fileuploader.min');
?>
<div class="wrap">
    <form>
        <label class="col-md-4" for="uploaded">Upload Document</label>
        <div class="col-md-4">
            <input type="file" id="uploaded" name="uploaded" style="display: inline-block;" >
        </div>
        <div class="col-md-4">
            <button class="btn btn-primary" data-id="upload" type="button" id="upload">Upload</button>
        </div>
    </form>
</div>
