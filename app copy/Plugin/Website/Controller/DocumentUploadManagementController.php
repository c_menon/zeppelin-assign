<?php

App::uses('CakeEmail', 'Network/Email');

class DocumentUploadManagementController extends WebsiteAppController {

    public $components = array('RequestHandler', 'Aws', 'CommonManager', 'Qimage');
    public $uses = array('DocumentType', 'Upload', 'ImageResolution', 'CompressedImageUpload');

    /*
     * Function to get Upload Document View
     */

    public function uploadDocumentView() {
        
    }
    
    public function viewImages(){
        
    }

    public function receiveUploadData() {
        $response = array();
        $requestDetails = $this->request->input('json_decode');
        $requestDetails = $this->objectToArray($requestDetails);

        $fileType = $requestDetails['file_type'];
        $uploadedFileName = $requestDetails['uploaded_file'];
        $uploadedFileData = $requestDetails['uploaded_file_data'];

        if (isset($uploadedFileName) && !empty($uploadedFileName) && isset($uploadedFileData) && !empty($uploadedFileData)) {

            list($contentType, $data) = explode(';', $uploadedFileData);
            $ufileData = str_replace("base64,", "", $data);

            // file structure in AWS
            $awsFolder = 'Local/Uploads/' . date('Y') . '/' . date('M') . '/' . date('d') . '/' . $uploadedFileName;
            // upload to AWS
            $contentType = explode('data:', $contentType)[1];
            $uploadedAwsUrl = $this->uploadDocumentsInAWS($contentType, $ufileData, $awsFolder);


            if ($uploadedAwsUrl) {
                $tableToAccess = 'Upload';
                $uploadId = $this->createMainUploadArray($uploadedFileName, $uploadedAwsUrl, $fileType, $tableToAccess);
                if ($uploadId) {
                    if ($this->checkImageCompression($contentType, $uploadedFileName, $ufileData, $uploadId)) {
                        $response = ["status" => "OK", "message" => "Document succesfully uploaded", "status_code" => 200];
                    } else {
                        $response['data'] = 'FAILURE';
                        $response['message'] = 'Compressed Image Save Failed';
                    }
                } else {
                    $response['data'] = 'FAILURE';
                    $response['message'] = 'Main document upload failed';
                }
            } else {
                $response['data'] = 'FAILURE';
                $response['message'] = 'Upload to AWS failed';
            }
        }
        $this->responseData = $response;
        $this->sendJsonResponse();
    }

    public function uploadDocumentsInAWS($fileTypeComp, $ufileData, $awsFolder) {
        $this->Aws->bucket = 'qtrove-seller-files';
        $uploadData = $this->Aws->upload($fileTypeComp, $ufileData, $awsFolder);
        if (!$uploadData) {
            $this->log('AWS update failed for ' . $fileName);
            return false;
        } else {
            return $uploadData['ObjectURL'];
        }
    }

    public function createMainUploadArray($uploadedFileName, $uploadedAwsUrl, $fileType, $tableToAccess) {
        $uploadArray = array();
//        $db = ConnectionManager::getDataSource('default');
//        $db->begin();
        $uploadTypeId = $this->DocumentType->getTypeId($fileType);

        $uploadArray['name'] = $uploadedFileName;
        $uploadArray['type_id'] = $uploadTypeId;
        $uploadArray['aws_url'] = $uploadedAwsUrl;
        $uploadArray['created_at'] = date('Y-m-d H:i:s');
        $uploadArray['updated_at'] = date('Y-m-d H:i:s');
        $uploadId = $this->saveToDatabase($uploadArray, $tableToAccess);
        if ($uploadId) {
            return $uploadId;
        }
        return false;
    }

    public function saveToDatabase($uploadArray, $tableToAccess) {
        $dataReturned = $this->$tableToAccess->saveData($uploadArray);
        if ($dataReturned) {
            return $dataReturned;
        }
        return false;
    }

    public function checkImageCompression($contentType, $uploadedFileName, $uploadedFileData, $uploadId) {
        if (is_int(strpos($contentType, 'image'))) {
            $filePath = WWW_ROOT . 'Uploads/' . $uploadedFileName;
            $imageResolutions = $this->ImageResolution->getImageResolutions();
            foreach ($imageResolutions as $imageResolutionsInst) {
                file_put_contents($filePath, base64_decode($uploadedFileData));
                $data = $this->getDataArray($uploadedFileName, $imageResolutionsInst['width'], $imageResolutionsInst['height']);
                if ($this->Qimage->resize($data)) {
                    $awsFolder = 'Local/Uploads/Compressed/' . $imageResolutionsInst['width'] . "-" . $imageResolutionsInst['height'] . "/" . date('Y') . '/' . date('M') . '/' . date('d') . '/' . $uploadedFileName;
                    $uploadedAwsUrl = $this->uploadDocumentsInAWS($contentType, base64_encode($filePath), $awsFolder);
                    if ($uploadedAwsUrl) {
                        $tableToAccess = 'CompressedImageUpload';
                        $compImageArray = $this->createUploadCompArray($uploadId, $uploadedAwsUrl, $imageResolutionsInst['id']);
                        $returnData = $this->saveToDatabase($compImageArray, $tableToAccess);
                        if (!$returnData) {
                            return false;
                        }
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
                unlink($filePath);
            }
        }
        return true;
    }

    public function getDataArray($uploadedFileName, $comWidth, $comHeight) {
        $data = array();

        $data['file'] = WWW_ROOT . 'Uploads/' . $uploadedFileName;
        $data['width'] = $comWidth;
        $data['height'] = $comHeight;
        $data['output'] = WWW_ROOT . 'Uploads/';
        $data['proportional'] = true;

        return $data;
    }

    public function createUploadCompArray($uploadId, $uploadedAwsUrl, $resolutionId) {
        $uploadCompArray = array();

        $uploadCompArray['upload_id'] = $uploadId;
        $uploadCompArray['aws_url'] = $uploadedAwsUrl;
        $uploadCompArray['resolution_id'] = $resolutionId;

        return $uploadCompArray;
    }

}
