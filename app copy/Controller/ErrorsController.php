<?php

App::uses('AppController', 'Controller');

class ErrorsController extends AppController
{
    public $components = array('RequestHandler', 'Auth');
    
//    public function beforeFilter() {
//        parent::beforeFilter($event);
//        $this->Auth->allow(['authenticationError']);
//    }
//    
//    public function authenticationError() {
//        $this->responseData = ['status' => 'failed', 'message' => 'API not authenticated', 'status_code' => 401];
//        $this->sendJsonResponse();
//        die();
//    }
//    
//    public function sendJsonResponse() {
//        if (isset($this->responseData['status_code']) && !empty($this->responseData['status_code'])) {
//            $statusCode = $this->responseData['status_code'];
//            $this->response->statusCode($statusCode);
//            unset($this->responseData['status_code']);
//        }
//        $this->autoRender = FALSE;
//        $this->response->type('json');
//        $this->response->body(json_encode($this->responseData));
//        $this->Logging->createLog("Response", "null",  $this->response);
//        $this->response->send();
//    }

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $this->set('bNErrors', $this->paginate($this->BNErrors));
        $this->set('_serialize', ['bNErrors']);
    }

    /**
     * View method
     *
     * @param string|null $id B N Error id.
     * @return void
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $bNError = $this->BNErrors->get($id, [
            'contain' => []
        ]);
        $this->set('bNError', $bNError);
        $this->set('_serialize', ['bNError']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $bNError = $this->BNErrors->newEntity();
        if ($this->request->is('post')) {
            $bNError = $this->BNErrors->patchEntity($bNError, $this->request->data);
            if ($this->BNErrors->save($bNError)) {
                $this->Flash->success(__('The b n error has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The b n error could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('bNError'));
        $this->set('_serialize', ['bNError']);
    }

    /**
     * Edit method
     *
     * @param string|null $id B N Error id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $bNError = $this->BNErrors->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $bNError = $this->BNErrors->patchEntity($bNError, $this->request->data);
            if ($this->BNErrors->save($bNError)) {
                $this->Flash->success(__('The b n error has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The b n error could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('bNError'));
        $this->set('_serialize', ['bNError']);
    }

    /**
     * Delete method
     *
     * @param string|null $id B N Error id.
     * @return void Redirects to index.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $bNError = $this->BNErrors->get($id);
        if ($this->BNErrors->delete($bNError)) {
            $this->Flash->success(__('The b n error has been deleted.'));
        } else {
            $this->Flash->error(__('The b n error could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
