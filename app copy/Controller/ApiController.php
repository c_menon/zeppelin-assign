<?php

App::uses('AppController', 'Controller');
//App::import('Controller', 'Errors');

/**
 * Api Controller
 *
 * Base class for Api
 */
App::uses('BaseAuthenticate', 'Controller/Component/Auth');

class ApiController extends AppController {

    public $uses = array('UserLogin', 'WebSession', 'Email');
    public $components = array('Auth', 'Cookie');

}
