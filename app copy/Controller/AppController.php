<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
    
    public function sendJsonResponse() {
        if (isset($this->responseData['status_code']) && !empty($this->responseData['status_code'])) {
            $statusCode = $this->responseData['status_code'];
            $this->response->statusCode($statusCode);
            unset($this->responseData['status_code']);
        }
        if (isset($this->responseData['message_code']) && !empty($this->responseData['message_code'])) {
            $this->responseData['message'] = __d('api', $this->responseData['message_code']);
            unset($this->responseData['message_code']);
        }
        $this->autoRender = FALSE;
        $this->response->type('json');
        $this->response->body(json_encode($this->responseData));
        $this->response->send();
        die();
    }
}
