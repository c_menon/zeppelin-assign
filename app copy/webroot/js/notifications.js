// Notification Script
$(document).ready(function()
{

    latestMessageId = $("#max_messageId")[0].getAttribute('value');

    $("#notificationLink").click(function()
    {

        $("#notificationContainer").fadeToggle(300);
        // $("#notification_count").fadeOut("slow");
        $("#notification_count").addClass('hidden');
        unReadNotificationCounter = 0;


        // use latestMessageId variable, user id and put on API for message read
        $.ajax({
            type: "POST",
            url: '/website/notification/updateLatestReadNotification',
            data: {latestMessageId: latestMessageId, userId: userId, companyId: companyId}


        });

        return false;
    });
    $(document).click(function()
    {

        $("#notificationContainer").hide();
    });
    $("#notificationContainer").click(function()
    {
        //return false;
    });
});



/*
 * Function to Change Elements on Receiving Notification
 */
function incrementNotification(messageText, url) {
    $('#noNotificationMessageWrapper').addClass('hidden');
    $('#notificationsPresentWrapper').removeClass('hidden');

    unReadNotificationCounter++;
    $('#notification_count').html(unReadNotificationCounter);
    $('#notification_count').removeClass('hidden');
    var previousNotificationsHtml = $('#notificationsPresentWrapper').html();

	if(messageText.length > 30){
		var limitedWidthMessageText = messageText.substring(0, 26) + "...";
	} else {
		var limitedWidthMessageText = messageText;
	}

    if (url != '') {
        var messageText = "<a href='" + url + "'>" + messageText + "</a>";
        var limitedWidthMessageText = "<a href='" + url + "'>" + limitedWidthMessageText + "</a>";
    }

    $('#notificationsPresentWrapper').html("<div class='notificationBox'>" + messageText + "</div>" + previousNotificationsHtml);
    $.notify('Notification', limitedWidthMessageText, 'message', '5000');
}

var pusher = new Pusher('d703bf3d9db1b6e43642');
var channel = pusher.subscribe('company_' + companyId + '_user_' + userId + '_channel'); // Change based on user id and company id
channel.bind('my_event', function(data) {
    if (data['floorId'] != 'undefined' && data['storeId'] != 'undefined' && data['customerId'] != 'undefined' && data['cX'] != 'undefined' && data['cY'] != 'undefined') {
        var url = "/website/configuration/showFloor?";
        url = url + "floorId=" + data['floorId'] + '&storeId=' + data['storeId'] + '&customerId=' + data['customerId'] + '&cx=' + data['cX'] + '&cy=' + data['cY'];

        if (data['pX'] != 'undefined' && data['pY'] != 'undefined') {
            url = url + '&px=' + data['pX'] + '&py=' + data['pY'];
        }
    } else {
        url = '/website/configuration/showFloor?floorId=1&storeId=1&cx=200&cy=200&customerId=1';
    }

    incrementNotification(data['text'], url);

    // update js variable latestMessageId to data['messageId']
    latestMessageId = data['messageId'];
});