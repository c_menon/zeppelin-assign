<?php

/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 */
App::uses('Model', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model {
    /*
     * Function to Add if the Conditions are not met with existing row
     * Similar to Insert Ignore, with ignore based on customizable conditions
     */

    public function add($conditions, $data) {
        if ($this->hasAny($conditions)) {
            return $this->find('first', array('conditions' => $conditions));
        } else {
            $this->create();
            return $this->save($data);
        }
    }

    /*
     * Function to Validate
     */

    public function validate($conditions) {
        if ($this->hasAny($conditions)) {
            return $this->find('first', array('conditions' => $conditions));
        } else {
            return false;
        }
    }

    /*
     * Function to Update
     */

    public function update($conditions, $updateData, $addData = null) {
        if ($this->hasAny($conditions)) {
            $this->updateAll($updateData, $conditions);
            return $this->validate($conditions);
        } else if (!empty($addData)) {
            $this->create();
            return $this->save($addData);
        } else {
            return false;
        }
    }
}
